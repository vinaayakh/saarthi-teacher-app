import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafePipe } from './safe/safe.pipe';
import { FormatTimePipe } from './formatTime/format-time.pipe';

@NgModule({
  declarations: [SafePipe, FormatTimePipe],
  exports: [SafePipe, FormatTimePipe],
  imports: [CommonModule],
})
export class PipesModule {}
