import { InjectionToken } from '@angular/core';
import { PPApiConfig } from './api.type';

export const PP_API_CONFIG = new InjectionToken<PPApiConfig>('NF_API_CONFIG');

export const viewAll = () => {
  return `programs/contents`;
};
