export enum PPApiResponseType {
  ARRAY_BUFFER = 'arraybuffer',
  BLOB = 'blob',
  JSON = 'json',
  TEXT = 'text',
}

export enum PPApiTokenType {
  BEARER = 'Bearer',
  BASIC = 'Basic',
}

export enum PPApiTokenStorageType {
  LOCAL_STORAGE = 'localstorage',
  COOKIE = 'cookie',
}
