import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ERROR_COMPONENT_TYPE} from "@angular/compiler";
import {ErrorPageComponent} from "./error-page/error-page.component";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/chats/chats.module').then(
      (m) => m.ChatsModule
    )
  },
  {
    path: '**',
    component: ErrorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
