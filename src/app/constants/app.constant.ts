import { BehaviorSubject } from 'rxjs';

export const CHANNEL: string = 'Production'; // Develop|Production|Staging
export const ORGANIZATION_ID: string = '5eb393ee95fab7468a79d189';
export let PAYMENT_METHOD: BehaviorSubject<string> =
  new BehaviorSubject<string>('RAZOR_PAY'); //RAZOR_PAY | PAYTM | INSTAMOJO | JUSPAY
export const SHARE_URL: string = 'https://physicswala.in';
export let PLAY_STORE_URL: string = 'https://bit.ly/2SHIPW6';
export const firebase = {
  apiKey: 'AIzaSyApsQyaMPr0NBBXi9OR7x7y0Kbc5qfLNc0',
  authDomain: 'physics-wallah-65ada.firebaseapp.com',
  databaseURL: 'https://physics-wallah-65ada.firebaseio.com',
  projectId: 'physics-wallah-65ada',
  storageBucket: 'physics-wallah-65ada.appspot.com',
  messagingSenderId: '252537344732',
  appId: '1:252537344732:web:59fbcdbca9e85979c4da19',
  measurementId: 'G-9RN512817V',
};

export function setPaymentMethod(paymentMethod: string) {
  PAYMENT_METHOD.next(paymentMethod);
}
