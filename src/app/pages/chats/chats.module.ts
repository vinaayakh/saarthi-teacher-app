import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatsRoutingModule } from './chats-routing.module';
import { UserChatComponent } from './user-chat/user-chat.component';
import { UserListComponent } from './user-list/user-list.component';
import { ChatsComponent } from './chats.component';


@NgModule({
  declarations: [
    UserChatComponent,
    UserListComponent,
    ChatsComponent
  ],
  imports: [
    CommonModule,
    ChatsRoutingModule
  ]
})
export class ChatsModule { }
